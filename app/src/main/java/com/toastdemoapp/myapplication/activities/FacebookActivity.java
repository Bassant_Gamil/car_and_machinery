package com.toastdemoapp.myapplication.activities;

import android.app.Service;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.ArraySet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.restrictivedatafilter.AddressFilterManager;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.toastdemoapp.myapplication.Fragment.HomeFragment;
import com.toastdemoapp.myapplication.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class FacebookActivity extends AppCompatActivity {
    TextView skipLogInTextView, facebookEmailTextView;
    LoginButton facebookLogInButton;
    ImageView facebookProfileImageView;


    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);
        facebookEmailTextView = findViewById(R.id.email_facebook_textView);
        facebookProfileImageView = findViewById(R.id.profile_picture_facebook_imageView);
        facebookLogInButton = findViewById(R.id.login_facebook_button);
        skipLogInTextView = findViewById(R.id.login_skip_TextView);
        skipLogInTextView.setOnClickListener(view -> {
            Intent i = new Intent(FacebookActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        });

        callbackManager = CallbackManager.Factory.create();
        facebookLogInButton.setPermissions(Arrays.asList("email", "public_profile"));
        facebookLogInButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code


            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        Intent i = new Intent(FacebookActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    AccessTokenTracker accessToken = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            if (currentAccessToken == null) {

                facebookEmailTextView.setText("");
                facebookProfileImageView.setImageResource(0);
                Toast.makeText(FacebookActivity.this, "User is logged out", Toast.LENGTH_LONG).show();
            } else
                loadUserProfile(currentAccessToken);


        }
    };

    private void loadUserProfile(AccessToken newAccessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String email = object.getString("email");
                    String id = object.getString("id");
                    String imageUrl = "http://graph.facebook.com/" + id + "/picture?type=normal";
                    facebookEmailTextView.setText(email);

                    Picasso.get()
                            .load(imageUrl)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            // .placeholder(R.drawable.img_placeholder)
                            .into(facebookProfileImageView);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email,id");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    public boolean checkConnection() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Service.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }
}
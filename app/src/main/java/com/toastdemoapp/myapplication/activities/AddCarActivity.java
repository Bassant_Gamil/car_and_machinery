package com.toastdemoapp.myapplication.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.toastdemoapp.myapplication.R;
import com.toastdemoapp.myapplication.backend.models.Car;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

import butterknife.ButterKnife;

public class AddCarActivity extends BaseActivity {
    Button addCarButton;

    private EditText modelEngEditText;
    private EditText makeEngEditText;
    private EditText modelArbEditText;
    private EditText makeArbEditText;
    private EditText priceEditText;
    private EditText lotEditText;
    private EditText bidsEditText;
    private TextView dateEditText;
    private TextView timeEditText;
    private ImageView carImageView;
    protected ImageView toolbarBackImageViewEng;
    protected ImageView toolbarBackImageViewAr;
    private SimpleDateFormat simpleDateFormat;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePicker;


    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String MODELEN = "modelEn";
    public static final String MAKEEN = "makeEn";
    public static final String MODELAR = "modelAr";
    public static final String MAKEAR = "makeAr";
    public static final String PRICE = "12356";
    public static final String IMAGE_URL = "imageURL";
    public static final String DATE = "1/1/2020";
    public static final Integer TIME = 0;
    public static final String LOT = "1";
    public static final String BIDS = "1";

    private String modelEn;
    private String makeEn;
    private String modelAr;
    private String makeAr;
    private String price;
    private String imageURL;
    private String date;
    private int time;
    private String lot;
    private String bids;

    private Uri photoUri;
    public long millisUntilFinished;
    ProgressDialog progressDialog;
    ArrayList<Car> cars;
    private AtomicLong timeMilliis;

    public AddCarActivity() {
        super(R.layout.activity_add_car, true);
    }

    @Override
    protected void doOnCreate(Bundle bundle) {
        ButterKnife.bind(this);
        modelArbEditText = findViewById(R.id.addCar_modelArb_editText);
        makeArbEditText = findViewById(R.id.addCar_makeArb_editText);
        modelEngEditText = findViewById(R.id.addCar_modelEng_editText);
        makeEngEditText = findViewById(R.id.addCar_makeEng_editText);
        priceEditText = findViewById(R.id.addCar_price_editText);
        lotEditText = findViewById(R.id.addCar_lot_editText);
        bidsEditText = findViewById(R.id.addCar_bids_editText);
        carImageView = findViewById(R.id.add_car_img_view);
        carImageView.setOnClickListener(view -> selectImage());
        addCarButton = findViewById(R.id.addCar_button);
        addCarButton.setOnClickListener(view -> {
            getInputData();
            saveData();
            Intent i = new Intent(AddCarActivity.this, MainActivity.class);
            startActivity(i);
        });
        simpleDateFormat = new SimpleDateFormat(" HH:mm", Locale.getDefault());

        dateEditText = findViewById(R.id.addCar_date_editText);
//        dateEditText.setOnClickListener(v -> {

//            Calendar cal = Calendar.getInstance();
//            final int year = cal.get(Calendar.YEAR);
//            final int month = cal.get(Calendar.MONTH);
//            final int day = cal.get(Calendar.DAY_OF_MONTH);
//            datePickerDialog = new DatePickerDialog(AddCarActivity.this, (datePicker, thisDay, thisMonth, thisYear) -> {
//                String chooseDate = thisDay + "_" + (thisMonth + 1) + "_" + thisYear;
//                dateEditText.setText(chooseDate);
//            }, year, month, day);
//            datePickerDialog.show();
//        });
//


        timeMilliis = new AtomicLong();
        timeEditText = findViewById(R.id.addCar_time_editText);
        timeEditText.setOnClickListener(view -> {
            Calendar time = Calendar.getInstance();
            int hour = time.get(Calendar.HOUR_OF_DAY) / 60;
            int minute = time.get(Calendar.MINUTE) / 60;
//            int second = time.get(Calendar.SECOND) / 1000 ;
            timePicker = new TimePickerDialog(AddCarActivity.this, (timePicker, selectedHour, selectedMinute) -> {
                String chooseTime = String.valueOf(selectedHour) + selectedMinute ;
                timeEditText.setText(chooseTime);
                timeMilliis.set(time.getTimeInMillis());
            }, hour, minute, false);
            timePicker.show();
//            run();
        });
        toolbarBackImageViewAr = findViewById(R.id.toolbar_forward_imageView);
        toolbarBackImageViewEng = findViewById(R.id.toolbar_back_imageView);
        if (Locale.getDefault().getLanguage().equals("en")) {
            toolbarBackImageViewEng.setVisibility(View.VISIBLE);
            toolbarBackImageViewEng.setOnClickListener(view -> finish());
        } else {
            toolbarBackImageViewAr.setVisibility(View.VISIBLE);
            toolbarBackImageViewAr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
//    @Override
//    public void run() {
//        long seconds = millisUntilFinished / 1000;
//        long minutes = seconds / 60;
//        long hours = minutes / 60;
//        long days = hours / 24;
//        String time = days+" "+"days" +" :" +hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
//        timeEditText.setText(time);
//        millisUntilFinished -= 1000;
//        Calendar timeCal = Calendar.getInstance();
//        int hour = timeCal.get(Calendar.HOUR_OF_DAY);
//        int minute = timeCal.get(Calendar.MINUTE);
//        timePicker = new TimePickerDialog(AddCarActivity.this, (timePicker, selectedHour, selectedMinute) ->{
//            String chooseTime = String.valueOf(selectedHour) + selectedMinute;
//                timeEditText.setText(chooseTime);
//                timeMilliis.set(time.getTimeInMillis());
//            }, hour, minute, false);
//            timePicker.show();
//        Log.d("DEV123",time);
////        timeEditText.setX(timeEditText.getX()+seconds);
//        /* and here comes the "trick" */
//        timeEditText.postDelayed(this, 1000);
//    }

    private void selectImage() {
        checkAndroidVersion();
        PickImageDialog.build(new PickSetup())
                .setOnPickResult(r -> {
                    photoUri = r.getUri();
                    imageURL = photoUri.toString();
                    Picasso.get()
                            .load(r.getUri())
                            .into(carImageView);
                    if (photoUri != null) {
                        progressDialog = new ProgressDialog(this);
                        progressDialog.setTitle("Uploading Image");
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("Please Wait");
                        progressDialog.show();
                    }
                    progressDialog.dismiss();
                })
                .setOnPickCancel(() -> {
                }).show(getSupportFragmentManager());
    }

    public void getInputData() {

        modelEn = modelEngEditText.getText().toString();
        makeEn = makeEngEditText.getText().toString();
        modelAr = modelArbEditText.getText().toString();
        makeAr = makeArbEditText.getText().toString();
        price = priceEditText.getText().toString();
        lot = lotEditText.getText().toString();
        bids = bidsEditText.getText().toString();
        date = dateEditText.getText().toString();
//        Car car = new Car(imageURL,model , price , date , time , lot , bids );
    }

    public void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (Locale.getDefault().getLanguage().equals("en")) {
            editor.putString(MODELEN, modelEn);
            editor.putString(MAKEEN, makeEn);
        } else {
            editor.putString(MODELAR, modelAr);
            editor.putString(MAKEAR, makeAr);
        }
        editor.putString(PRICE, price);
        editor.putString(IMAGE_URL, imageURL);
        editor.putString(DATE, date);
        editor.putInt(String.valueOf(TIME), timeMilliis.intValue());
        editor.putString(LOT, lot);
        editor.putString(BIDS, bids);
//        editor.putString(IMAGE_URL,imageURL);

        editor.apply();
        Toast.makeText(this, "Car added", Toast.LENGTH_LONG).show();
    }

    public void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 555);
            } catch (Exception e) {

            }
        } else {
            pickUpImage();
        }
    }

    private void pickUpImage() {
        CropImage.startPickImageActivity(this);
        Picasso.get()
                .load(photoUri)
                .into(carImageView);
    }

    private void cropRequest(Uri photoUri) {
        CropImage.activity(photoUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        return false;
    }

}

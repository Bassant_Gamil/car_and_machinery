package com.toastdemoapp.myapplication.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.PopupMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.toastdemoapp.myapplication.R;

import java.util.Locale;

public abstract class BaseActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    private static final String TAG = "Base_Activity";

    private Context context;
    private int mActivityLayout;
    private boolean showToolbar;
    // Main Layout
    protected ViewGroup contentLayout;
    // Toolbar
    protected Toolbar toolbar;
    protected TextView toolbarTextView;
    protected ImageView toolbarBackImageViewEng;
    protected ImageView toolbarBackImageViewAr;
    protected ImageView toolbarLanguage;

    public BaseActivity(int activityLayout, boolean showToolbar) {
        this.mActivityLayout = activityLayout;
        this.showToolbar = showToolbar;
    }

    /**
     * Use instead of onCreate in activities.
     *
     * @param bundle Bundle
     */
    protected abstract void doOnCreate(Bundle bundle);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_base);
        Log.v(TAG, "onCreate");
        initToolbar();

        contentLayout = findViewById(R.id.base_frameLayout);
        LayoutInflater.from(this).inflate(mActivityLayout, contentLayout, true);
        doOnCreate(savedInstanceState);

        if (showToolbar) {
            Log.v(TAG, "Show toolbar");
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
        }
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTextView = findViewById(R.id.toolbar_textView);
        toolbarBackImageViewAr = findViewById(R.id.toolbar_forward_imageView);
        toolbarBackImageViewEng = findViewById(R.id.toolbar_back_imageView);
        if (Locale.getDefault().getLanguage().equals("en")) {
            toolbarBackImageViewEng.setVisibility(View.VISIBLE);
            toolbarBackImageViewEng.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backIntent();
                }
            });
        } else {
            toolbarBackImageViewAr.setVisibility(View.VISIBLE);
            toolbarBackImageViewAr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backIntent();
                }
            });

        }
        toolbarLanguage = findViewById(R.id.toolbar_language);
        toolbarLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] language = {"English", "العربية"};
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(BaseActivity.this);
                mBuilder.setSingleChoiceItems(language, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            setLocale("en");
                            recreate();
                        } else {
                            setLocale("Ar");
                            recreate();
                        }
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });

        setSupportActionBar(toolbar);
    }

    public void backIntent() {
        Intent i = new Intent(BaseActivity.this, SplashActivity.class);
        startActivity(i);
        finish();
        System.exit(0);
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_lang", lang);
        editor.apply();
    }

    public void loadLocale() {
        SharedPreferences preferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = preferences.getString("My_lang", "");
        setLocale(language);
    }
}

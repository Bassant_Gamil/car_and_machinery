package com.toastdemoapp.myapplication.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.toastdemoapp.myapplication.Fragment.GavelFragment;
import com.toastdemoapp.myapplication.Fragment.HomeFragment;
import com.toastdemoapp.myapplication.Fragment.LikeFragment;
import com.toastdemoapp.myapplication.Fragment.MoreFragment;
import com.toastdemoapp.myapplication.Fragment.ShoppingFragment;
import com.toastdemoapp.myapplication.R;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener  {

    BottomNavigationView bottomNavigationView;


    public MainActivity() {
        super(R.layout.activity_main, true);
    }

    @Override
    protected void doOnCreate(Bundle bundle) {
        initBottomNavigation();

        toolbarTextView.setVisibility(View.VISIBLE);


        loadStartFragment();
    }

    private void initBottomNavigation() {
        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.getMenu();


        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setOnNavigationItemReselectedListener(this);
    }

    private void loadStartFragment() {
        loadFragment(new HomeFragment());
    }

    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        transaction.commit();
    }


    @Override
    public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                toolbarTextView.setText(R.string.toolbarText);
                loadFragment(new HomeFragment());
                return true;
            case R.id.navigation_gavel:
                toolbarTextView.setText(R.string.toolbarText);
                loadFragment(new GavelFragment());
                return true;
            case R.id.navigation_like:
                toolbarTextView.setText(R.string.toolbarText);
                loadFragment(new LikeFragment());
                return true;
            case R.id.navigation_shopping:
                toolbarTextView.setText(R.string.toolbarText);
                loadFragment(new ShoppingFragment());
                return true;
            case R.id.navigation_more:
                toolbarTextView.setText(R.string.toolbarText);
                loadFragment(new MoreFragment());
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        return false;
    }
}

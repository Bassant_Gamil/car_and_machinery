package com.toastdemoapp.myapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.toastdemoapp.myapplication.R;

import butterknife.BindView;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_textView)
    TextView splashTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashTextView = findViewById(R.id.car_text_view);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade);
        splashTextView.startAnimation(animation);

        int SPLASH_DISPLAY_LENGTH = 2000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, FacebookActivity.class);
                startActivity(i);
                finish();
            }
                }, SPLASH_DISPLAY_LENGTH);
        }
    }
package com.toastdemoapp.myapplication.backend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

public class AuctionInfo implements Serializable {

    @SerializedName("bids")
    @Expose
    private Integer bids;
    @SerializedName("endDate")
    @Expose
    private Integer endDate;
    @SerializedName("endDateEn")
    @Expose
    private String endDateEn;
    @SerializedName("endDateAr")
    @Expose
    private String endDateAr;
    @SerializedName("currencyEn")
    @Expose
    private String currencyEn;
    @SerializedName("currencyAr")
    @Expose
    private String currencyAr;
    @SerializedName("currentPrice")
    @Expose
    private Integer currentPrice;
    @SerializedName("minIncrement")
    @Expose
    private Integer minIncrement;
    @SerializedName("lot")
    @Expose
    private Integer lot;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("VATPercent")
    @Expose
    private Integer vATPercent;
    @SerializedName("itemAuctionType")
    @Expose
    private Integer itemAuctionType;
    @SerializedName("isModified")
    @Expose
    private Integer isModified;
    @SerializedName("itemid")
    @Expose
    private Integer itemid;
    @SerializedName("iCarId")
    @Expose
    private Integer iCarId;
    @SerializedName("iVinNumber")
    @Expose
    private String iVinNumber;

    public static final Comparator<AuctionInfo> BY_PRICE = new Comparator<AuctionInfo>() {
        @Override
        public int compare(AuctionInfo auctionInfo, AuctionInfo t1) {
            return auctionInfo.currentPrice.compareTo(t1.currentPrice);
        }
    };

    public AuctionInfo(Integer bids, Integer endDate, Integer currentPrice, Integer lot) {
        this.bids = bids;
        this.endDate = endDate;
        this.currentPrice = currentPrice;
        this.lot = lot;
    }

    public AuctionInfo(Integer bids, Integer endDate, String endDateEn, String endDateAr,
                       String currencyEn, String currencyAr, Integer currentPrice, Integer minIncrement,
                       Integer lot, Integer priority, Integer vATPercent, Integer itemAuctionType,
                       Integer isModified, Integer itemid, Integer iCarId, String iVinNumber) {
        this.bids = bids;
        this.endDate = endDate;
        this.endDateEn = endDateEn;
        this.endDateAr = endDateAr;
        this.currencyEn = currencyEn;
        this.currencyAr = currencyAr;
        this.currentPrice = currentPrice;
        this.minIncrement = minIncrement;
        this.lot = lot;
        this.priority = priority;
        this.vATPercent = vATPercent;
        this.itemAuctionType = itemAuctionType;
        this.isModified = isModified;
        this.itemid = itemid;
        this.iCarId = iCarId;
        this.iVinNumber = iVinNumber;
    }

    public Integer getBids() {
        return bids;
    }

    public void setBids(Integer bids) {
        this.bids = bids;
    }

    public Integer getEndDate() {
        return endDate;
    }

    public void setEndDate(Integer endDate) {
        this.endDate = endDate;
    }

    public String getEndDateEn() {
        return endDateEn;
    }

    public void setEndDateEn(String endDateEn) {
        this.endDateEn = endDateEn;
    }

    public String getEndDateAr() {
        return endDateAr;
    }

    public void setEndDateAr(String endDateAr) {
        this.endDateAr = endDateAr;
    }

    public String getCurrencyEn() {
        return currencyEn;
    }

    public void setCurrencyEn(String currencyEn) {
        this.currencyEn = currencyEn;
    }

    public String getCurrencyAr() {
        return currencyAr;
    }

    public void setCurrencyAr(String currencyAr) {
        this.currencyAr = currencyAr;
    }

    public Integer getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Integer currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Integer getMinIncrement() {
        return minIncrement;
    }

    public void setMinIncrement(Integer minIncrement) {
        this.minIncrement = minIncrement;
    }

    public Integer getLot() {
        return lot;
    }

    public void setLot(Integer lot) {
        this.lot = lot;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getVATPercent() {
        return vATPercent;
    }

    public void setVATPercent(Integer vATPercent) {
        this.vATPercent = vATPercent;
    }

    public Integer getItemAuctionType() {
        return itemAuctionType;
    }

    public void setItemAuctionType(Integer itemAuctionType) {
        this.itemAuctionType = itemAuctionType;
    }

    public Integer getIsModified() {
        return isModified;
    }

    public void setIsModified(Integer isModified) {
        this.isModified = isModified;
    }

    public Integer getItemid() {
        return itemid;
    }

    public void setItemid(Integer itemid) {
        this.itemid = itemid;
    }

    public Integer getICarId() {
        return iCarId;
    }

    public void setICarId(Integer iCarId) {
        this.iCarId = iCarId;
    }

    public String getIVinNumber() {
        return iVinNumber;
    }

    public void setIVinNumber(String iVinNumber) {
        this.iVinNumber = iVinNumber;
    }
}

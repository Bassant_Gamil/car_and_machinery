package com.toastdemoapp.myapplication.backend.api;

import com.toastdemoapp.myapplication.backend.models.Cars;


import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    /**
     * ---------------------------------------------------------------------------------------------
     * -------------------------------------- USER -------------------------------------------------
     * ---------------------------------------------------------------------------------------------
     * @return
     */

//    @POST(ApiClient.BASE_URL + Constants.SERVICES_PUSH_REFRESH_TOKEN)
//    Call<Token> doRefreshToken(@HeaderMap Map<String, String> headers,
//                               @Body Map<String, Object> params);

    @GET("carsonline")
    Call<Cars> doCar();
}


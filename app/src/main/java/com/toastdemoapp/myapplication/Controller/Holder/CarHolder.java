package com.toastdemoapp.myapplication.Controller.Holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.toastdemoapp.myapplication.R;
import com.toastdemoapp.myapplication.backend.models.Car;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;


public class CarHolder extends RecyclerView.ViewHolder {
    public Context context;
    public ImageView carImageView;
    public ImageView favouriteImageView;
    public TextView modelTextView;
    public TextView priceTextView;
    public TextView numLotTextView;
    public TextView numBidsTextView;
    public TextView numTimeTextView;
    boolean status;
    String imageUrl;
    long millisUntilFinished;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String MODEL = "model";
    public static final String PRICE = "12356";
    public static final String IMAGE_URL = "imageURL";
    public static final String DATE = "1/1/2020";
    public static final String TIME = "12:00";
    public static final String LOT = "1";
    public static final String BIDS = "1";

    private String model;
    private String price;
    private String imageURL;
    private String date;
    private String time;
    private String lot;
    private String bids;

    public CarHolder(View view, String type) {
        super(view);

        if (type.equals("Grid")) {

            carImageView = view.findViewById(R.id.car_grid_imageView);
            favouriteImageView = view.findViewById(R.id.favourite_grid_imageView);
            modelTextView = view.findViewById(R.id.model_grid_textView);
            priceTextView = view.findViewById(R.id.price_grid_textView);
            numLotTextView = view.findViewById(R.id.num_lot_grid_textView);
            numBidsTextView = view.findViewById(R.id.num_bids_grid_textView);
            numTimeTextView = view.findViewById(R.id.num_time_grid_textView);
        } else {
            carImageView = view.findViewById(R.id.car_list_imageView);
            favouriteImageView = view.findViewById(R.id.favourite_list_imageView);
            modelTextView = view.findViewById(R.id.model_list_textView);
            priceTextView = view.findViewById(R.id.price_list_textView);
            numLotTextView = view.findViewById(R.id.num_lot_list_textView);
            numBidsTextView = view.findViewById(R.id.num_bids_list_textView);
            numTimeTextView = view.findViewById(R.id.num_time_list_textView);
        }
    }

    public void bind(Car car) {


//        modelTextView.setText(model);
//        priceTextView.setText(price);
//        numLotTextView.setText(lot);
//        numBidsTextView.setText(bids);
//        numTimeTextView.setText(time);


        imageUrl = car.getImage();
        imageUrl = imageUrl.replace("[w]", "0").replace("[h]", "0");
        if (car.getLocal() == null) {
            Picasso.get()
                    .load(imageUrl)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .placeholder(R.drawable.img_placeholder)
                    .into(carImageView);
        } else {
            Picasso.get()
                    .load(imageURL)
                    .into(carImageView);
        }
        if (Locale.getDefault().getLanguage().equals("en"))
            modelTextView.setText(car.getMakeEn() + " " + car.getModelEn());
        else
            modelTextView.setText(car.getMakeAr() + " " + car.getModelAr());

        priceTextView.setText(String.valueOf(car.getAuctionInfo().getCurrentPrice()));
        numLotTextView.setText(String.valueOf(car.getAuctionInfo().getLot()));
        numBidsTextView.setText(String.valueOf(car.getAuctionInfo().getBids()));


//        final Boolean favorite = CarAdapter.setCars();
//
//        if (favorite){
//            favouriteImageView.setImageResource(R.drawable.ic_favorite_red);
//        } else {
//            favouriteImageView.setImageResource(R.drawable.ic_favorite_border_white);
//        }
//
        favouriteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (status == false) {
                    favouriteImageView.setImageResource(R.drawable.ic_favorite_red);
                    status = true;
                    favouriteImageView.setSaveFromParentEnabled(true);
                    return;

                } else {
                    favouriteImageView.setImageResource(R.drawable.ic_favorite_border_white);
                    status = false;
                    favouriteImageView.setSaveFromParentEnabled(true);
                    return;
                }
            }
        });

        long timer = Long.parseLong(String.valueOf(car.getAuctionInfo().getEndDate()));

        timer = timer * 1000;
        new CountDownTimer(timer ,  1000) {
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                String time = String.format("%02d:%02d:%02d", hours % 24, minutes % 60, seconds % 60);
                numTimeTextView.setText(time);
                millisUntilFinished -=1000;

                if(millisUntilFinished <=300000) {
                    numTimeTextView.setTextColor(Color.RED);
                } else

                {
                    numTimeTextView.setTextColor(Color.BLACK);
                }
//                /* and here comes the "trick" */
//                numTimeTextView.postDelayed(this,1000);



            }

            public void onFinish() {
                numTimeTextView.setText("00:00:00");
            }
        }.start();



//    long seconds = millisUntilFinished / 1000;
//    long minutes = seconds / 60;
//    long hours = minutes / 60;
//    String time = String.format("%02d:%02d:%02d", hours, minutes % 60, seconds % 60);
//    numTimeTextView.setText(time);


//        millisUntilFinished -=1000;
//
//        if(millisUntilFinished <=300000)

//    {
//        numTimeTextView.setTextColor(Color.RED);
//    } else
//
//    {
//        numTimeTextView.setTextColor(Color.BLACK);
//    }
//    /* and here comes the "trick" */
//        numTimeTextView.postDelayed(this,1000);
//

    }
}

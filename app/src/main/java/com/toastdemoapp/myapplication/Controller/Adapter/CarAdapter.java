package com.toastdemoapp.myapplication.Controller.Adapter;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import com.toastdemoapp.myapplication.Controller.Holder.CarHolder;
import com.toastdemoapp.myapplication.R;
import com.toastdemoapp.myapplication.backend.models.Car;

import java.util.List;


public class CarAdapter extends RecyclerView.Adapter<CarHolder> implements Filterable {

    private List<Car> cars;
    private Context context;
    private String type;
    CountDownTimer timer;

    public CarAdapter(List<Car> cars, Context context , String type) {
        this.cars = cars;
        this.context = context;
        this.type = type;
    }

    public Context getContext() {
        return context;
    }

    @NonNull
    @Override
    public CarHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v ;

        if (type.equals("Grid")){
            v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_car_grid_view, viewGroup, false);
        } else {
            v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_car_portrait, viewGroup, false);

        }
        return new CarHolder(v , type);
    }


    @Override
    public void onBindViewHolder(@NonNull CarHolder carHolder, int i) {
        Car car = cars.get(i);

        carHolder.bind(car);
    }
    @Override
    public int getItemCount() {
        return cars.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
        notifyDataSetChanged();
    }
}

package com.toastdemoapp.myapplication.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.toastdemoapp.myapplication.Controller.Adapter.CarAdapter;
import com.toastdemoapp.myapplication.R;
import com.toastdemoapp.myapplication.activities.AddCarActivity;
import com.toastdemoapp.myapplication.backend.api.ApiClient;
import com.toastdemoapp.myapplication.backend.api.ApiInterface;
import com.toastdemoapp.myapplication.backend.models.AuctionInfo;
import com.toastdemoapp.myapplication.backend.models.Car;
import com.toastdemoapp.myapplication.backend.models.Cars;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemSelectedListener,
        PopupMenu.OnMenuItemClickListener {
    private Context context;
    private RecyclerView recyclerViewHomeFragment;

    private Spinner spinner;
    private ArrayAdapter<String> carModelAdapter;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    List<Car> cars;
    private List<String> carModel;
    ProgressBar loadMoreProgressBar;
    View noResultsView;
    View loadingView;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView endResultsTextView;
    Car car;
    CarAdapter carAdapter;

    Button filterButton;
    Button sortButton;
    Button gridViewButton;


    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String MODELEN = "modelEn";
    public static final String MAKEEN = "makeEn";
    public static final String MODELAR = "modelAr";
    public static final String MAKEAR = "makeAr";
    public static final String PRICE = "12356";
    public static final String IMAGE_URL = "imageURL";
    public static final String DATE = "1-1-2020";
    public static final Integer TIME = 0;
    public static final String LOT = "1";
    public static final String BIDS = "1";

    private Boolean isList = true;

    private boolean isSelected = false;
    private String modelEn;
    private String makeEn;
    private String modelAr;
    private String makeAr;
    private String price;
    private String imageURL;
    private String date;
    private int time;
    private String lot;
    private String bids;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity().getApplicationContext();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        Gson gson = new Gson();
        if (Locale.getDefault().getLanguage().equals("en")) {
            modelEn = sharedPreferences.getString(MODELEN, "");
            makeEn = sharedPreferences.getString(MAKEEN, "");
        } else {
            modelAr = sharedPreferences.getString(MODELAR, "");
            makeAr = sharedPreferences.getString(MAKEAR, "");
        }
        price = sharedPreferences.getString(PRICE, String.valueOf(1));
        date = sharedPreferences.getString(DATE, String.valueOf(1));
        time = sharedPreferences.getInt(String.valueOf(TIME), 0);
        lot = sharedPreferences.getString(LOT, String.valueOf(1));
        bids = sharedPreferences.getString(BIDS, String.valueOf(1));
        imageURL = sharedPreferences.getString(IMAGE_URL, "");
//        Picasso.get()
//                .load(imageURL)
//                .into();
//        Type type = new TypeToken<ArrayList<Car>>() {
//        }.getType();
//        car = gson.fromJson(imageURL, type);
//        car = gson.fromJson(model, type);
//        car = gson.fromJson(price, type);
//        car = gson.fromJson(date, type);
//        car = gson.fromJson(time, type);
//        car = gson.fromJson(lot, type);
//        car = gson.fromJson(bids, type);
//        if (cars == null) {
//            cars = new ArrayList<>();
//        }

        AuctionInfo auctionInfo = new AuctionInfo(Integer.valueOf(bids), time,
                Integer.valueOf(price), Integer.valueOf(lot));
        if (Locale.getDefault().getLanguage().equals("en")) {

            car = new Car(true, imageURL, modelEn, makeEn, auctionInfo);
        } else {
            car = new Car(true, imageURL, modelAr, makeAr, auctionInfo);
        }

        recyclerViewHomeFragment = view.findViewById(R.id.fragment_home_recyclerView);
        loadMoreProgressBar = view.findViewById(R.id.loadMore_progressBar);
        noResultsView = view.findViewById(R.id.no_results_found_view);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        endResultsTextView = view.findViewById(R.id.end_results_textView);
        loadingView = view.findViewById(R.id.loading_view);
        ImageButton addCarFab = view.findViewById(R.id.add_car_imageButton);
        addCarFab.setOnClickListener(view1 -> {

            Intent i = new Intent(HomeFragment.this.getActivity(), AddCarActivity.class);
            startActivity(i);
        });

        filterButton = view.findViewById(R.id.filter_button);
//        Drawable drawableImageViewFilter =filterButton.getContext().getResources().getDrawable(R.drawable.ic_filter_list_black);
//        filterButton.setCompoundDrawablesWithIntrinsicBounds(drawableImageViewFilter,null,null,null);

        sortButton = view.findViewById(R.id.sort_button);

        gridViewButton = view.findViewById(R.id.grid_view_button);

        apiRequest();
        sortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, view);
                popupMenu.setOnMenuItemClickListener(HomeFragment.this);
                popupMenu.inflate(R.menu.sort_search);
                popupMenu.show();
            }
        });


        gridViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isList) {
                    isList = false;

                    carAdapter = new CarAdapter(cars, context, "Grid");
                    recyclerViewHomeFragment.setLayoutManager(new GridLayoutManager(context, 2));
                    recyclerViewHomeFragment.setAdapter(carAdapter);
                    Drawable drawableImageViewGrid = gridViewButton.getContext().getResources().getDrawable(R.drawable.ic_view_list_black);
                    gridViewButton.setCompoundDrawablesRelativeWithIntrinsicBounds(drawableImageViewGrid, null, null, null);
                    gridViewButton.setText(getString(R.string.list_view));

                } else {
                    isList = true;

                    carAdapter = new CarAdapter(cars, context, "Normal");
                    recyclerViewHomeFragment.setLayoutManager(new LinearLayoutManager(context));
                    recyclerViewHomeFragment.setAdapter(carAdapter);
                    Drawable drawableImageViewList = gridViewButton.getContext().getResources().getDrawable(R.drawable.ic_view_grid_black);
                    gridViewButton.setCompoundDrawablesRelativeWithIntrinsicBounds(drawableImageViewList, null, null, null);
                    gridViewButton.setText(getString(R.string.grid_view));

                }
            }
        });

        filterButton.setOnClickListener(view12 -> openGroupDialog());

        swipeRefreshLayout.setOnRefreshListener(() -> {
            apiRequest();
            swipeRefreshLayout.setRefreshing(false);
        });


        runSwipeRefreshLayout();


        return view;
    }


    private void runSwipeRefreshLayout() {
        loadingView.setVisibility(View.VISIBLE);
        endResultsTextView.setVisibility(View.GONE);

        recyclerViewHomeFragment.setAdapter(null);
    }


    private void apiRequest() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        apiService.doCar().enqueue(new Callback<Cars>() {
            @Override
            public void onResponse(Call<Cars> call, Response<Cars> response) {
                cars = response.body().getCar();
                cars.add(0, car);

                if (isList) {
                    carAdapter = new CarAdapter(cars, context, "Normal");
                    recyclerViewHomeFragment.setLayoutManager(new LinearLayoutManager(context));
                    recyclerViewHomeFragment.setAdapter(carAdapter);
                } else {
                    carAdapter = new CarAdapter(cars, context, "Grid");
                    recyclerViewHomeFragment.setLayoutManager(new GridLayoutManager(context, 2));
                    recyclerViewHomeFragment.setAdapter(carAdapter);
                }

                endResultsTextView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                loadingView.setVisibility(View.GONE);
                loadMoreProgressBar.setVisibility(View.GONE);

                getCarModel(response.body().getCar());
            }

            @Override
            public void onFailure(Call<Cars> call, Throwable t) {
                Log.v("FAILURE_OUTPUT", t.getMessage());

            }

        });
    }


    @Override
    public void onRefresh() {
        endResultsTextView.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    public void openGroupDialog() {
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.choose_model_dialog, null);

        spinner = dialogView.findViewById(R.id.model_spinner);
        carModelAdapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, carModel);
        spinner.setOnItemSelectedListener(this);
        spinner.setAdapter(carModelAdapter);
        builder.setView(dialogView);
        dialogView.findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                carAdapter.setCars(cars);
                isSelected = false;
            }
        });
        dialog = builder.create();
        dialog.show();

    }

    public void getCarModel(List<Car> cars) {
        carModel = new ArrayList<>();
        for (Car car : cars) {
            carModel.add(car.getModelEn());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView.getId() == R.id.model_spinner && isSelected) {
            List<Car> filteredCar = new ArrayList<>();
            filteredCar.add(cars.get(i));
            carAdapter.setCars(filteredCar);
            dialog.dismiss();
        }
        isSelected = !isSelected;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.endYear:
                Collections.sort(cars, new Comparator<Car>() {
                    @Override
                    public int compare(Car car, Car t1) {
                        return car.getAuctionInfo().getEndDateEn().compareTo(t1.getAuctionInfo().getEndDateEn());
                    }
                });
                carAdapter.notifyDataSetChanged();
                break;
            case R.id.price:
                Collections.sort(cars, new Comparator<Car>() {
                    @Override
                    public int compare(Car car, Car t1) {
                        return car.getAuctionInfo().getCurrentPrice().compareTo(t1.getAuctionInfo().getCurrentPrice());
                    }
                });
                carAdapter.notifyDataSetChanged();
                break;
            case R.id.year:
                Collections.sort(cars, new Comparator<Car>() {
                    @Override
                    public int compare(Car car, Car t1) {
                        return car.getYear().compareTo(t1.getYear());
                    }
                });
                carAdapter.notifyDataSetChanged();
                break;
            default:
                return false;
        }

        return false;
    }
}
